package com.assignment1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import java.io.*;
import java.net.URI;

public class FileUploadHDFS {

    public static final String LOCAL_PATH = System.getProperty("user.home") + "/java_workspace/HdfsHBaseFileTx/src/main/resources/Assignment-1-Input_Files";
    public static final String URL = "hdfs://localhost:9000";
    public static final String HDFS_URI = "hdfs://localhost:9000/User/Files";

    public static void main(String args[]) throws IOException {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(URI.create(URL), conf);
        for (int i = 0; i < 100; i++) {
            String FilePath = LOCAL_PATH + "/File_" + i + ".csv";
            fs.copyFromLocalFile(new Path(FilePath), new Path(HDFS_URI));
        }
    }
}
