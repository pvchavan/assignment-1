package com.assignment1;

import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.net.URI;
import java.util.HashMap;

public class HdFStoHBase {

    public static final String encoding = "UTF-8";
    public static final String rootUrl = "hdfs://localhost:9000";
    public static String internalUrl = "/User/Files";
    private static String columnFamily = "Name";
    private static Configuration config = new Configuration();

    public static void main(String[] args) throws Exception {
        FileSystem fs = getInputFileSystem();
        String uri = getPath(internalUrl);
        HashMap<Integer, String> hm = getColumnMapping();
        createTable("People_Info");
        storeDataInHbase(fs, uri, hm);
    }

    public static String getPath(String url) {
        return rootUrl + url;
    }

    private static void createTable(String tableName) throws Exception {
        Configuration conf = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(conf);
        HBaseAdmin hAdmin = (HBaseAdmin) connection.getAdmin();
        if (hAdmin.tableExists(TableName.valueOf(tableName))) {
            System.out.println(tableName + " already exists");
            return;
        }
        TableName tName = TableName.valueOf(tableName);
        TableDescriptorBuilder tableDescBuilder = TableDescriptorBuilder.newBuilder(tName);
        tableDescBuilder.setColumnFamily(ColumnFamilyDescriptorBuilder.newBuilder("Name".getBytes()).build())
                .setColumnFamily(ColumnFamilyDescriptorBuilder.newBuilder("Age".getBytes()).build())
                .setColumnFamily(ColumnFamilyDescriptorBuilder.newBuilder("Company".getBytes()).build())
                .setColumnFamily(ColumnFamilyDescriptorBuilder.newBuilder("Building_code".getBytes()).build())
                .setColumnFamily(ColumnFamilyDescriptorBuilder.newBuilder("Phone_Number".getBytes()).build())
                .setColumnFamily(ColumnFamilyDescriptorBuilder.newBuilder("Address".getBytes()).build())
                .build();

        hAdmin.createTable(tableDescBuilder.build());
        System.out.println(tableName + "table crated");
    }


    private static void insertDataToHbase(String[] record,
                                          int rowId,
                                          HashMap<Integer, String> hm) throws Exception {
        Table table = null;
        Connection connection = null;
        try {
            Configuration conf = HBaseConfiguration.create();
            connection = ConnectionFactory.createConnection(conf);
            table = connection.getTable(TableName.valueOf("People_Info"));
            Put p = new Put(Bytes.toBytes(String.valueOf(rowId)));
            for (int i = 0; i < record.length; ++i) {
                String qualifier = hm.get(i);
                if (qualifier != null) {
                    p.addColumn(Bytes.toBytes(columnFamily),
                            Bytes.toBytes(qualifier),
                            Bytes.toBytes(record[i]));
                }
            }
            table.put(p);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (table != null)
                table.close();
            if (connection != null)
                connection.close();
        }
    }

    private static FileSystem getInputFileSystem() throws Exception {
        try {
            FileSystem fs = FileSystem.get(new URI(rootUrl), config);
            return fs;
        } catch (Exception e) {
            throw new Exception("Invalid HDFS path");
        }
    }

    private static void storeDataInHbase(FileSystem hdfs,
                                         String uri,
                                         HashMap<Integer, String> hm) throws Exception {

        config.set("fs.defaultFS", rootUrl);
        FileSystem fileSystem = FileSystem.get(config);
        FileStatus[] fileStatus = hdfs.listStatus(new Path(uri));
        Path[] paths = FileUtil.stat2Paths(fileStatus);
        int rowId = 1;
        for (Path path : paths) {
            FSDataInputStream inputStream = fileSystem.open(path);
            String out = IOUtils.toString(inputStream, encoding).split("\n")[1];
            String[] record = out.split(",");
            insertDataToHbase(record, rowId, hm);
            rowId++;
            inputStream.close();
        }
        fileSystem.close();
    }

    public static HashMap<Integer, String> getColumnMapping() {
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(0, "Name");
        hm.put(1, "Age");
        hm.put(2, "Company");
        hm.put(3, "Building_code");
        hm.put(4, "Phone_Number");
        hm.put(5, "Address");
        return hm;
    }
}

